# Taskify #

### Bitbucket Repository Link ###

**Clone Link**
- https://Chris_Fortune@bitbucket.org/Chris_Fortune/taskify.git

**Repo Link**
- [Click here to view repo on BitBucket.org.](https://bitbucket.org/Chris_Fortune/taskify/src/master/)

### Installation Instructions ###

* To run Taskify, you just need to open the project and run the App on an Android Wear 2.0 Device or emulator. 

### Hardware Considerations ###

* Currently, Taskify is set to run at a minimum SDK Version 23 and a compile SDK Version 28 and supports round and square watchfaces.

### List of Known Bugs ###

* Currently there are know bugs known at the moment.

### Where to Find Weekly Requirements ###

**Home Screen**

- List of all the tasks the user has created is being presented to the user in the ListView.
- Tapping on a task will take the user to the Selected Task Screen displaying the task chosen. 
- Long pressing on a task will complete a task and put a line through the text along with changing the text color to green.
- Pressing the up arrow will take the user to the options screen.

**Selected Task Screen**

- Displays the task the user selected in a Text View.
- Back button to allow the user to go back to the home screen.
- Complete button to allow the user to complete the task and send the user back to the home screen.
- Edit Task Button to allow the user to edit the selected task.
- Delete Button to allow the user to delete the task selected.

**Edit Task Screen**

- Allows the user to edit the selected task and save it.
- Save button to save the selected task and makes sure the user has entered valuable information.
- Back button to send the user back to the selected task screen.

**Options Screen**

- Button to Add a task will take the user to the Add Task Screen.
- Button to Delete All Tasks will delete all the tasks the user has created and take the user back to the home screen.
- Button to Complete All tasks will complete all tasks and send the user back to the home screen.
- Button to See Completed Task Count  will take the user to the Completed Task Count screen.

**Completed Task Screen** 

- Displays the count of the completed Tasks.
- Button to allow the user to go back to the options screen.
